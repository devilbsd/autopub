# AuToPub #

Automatic Tools for Publisher, is a fabric´s fabfile that automates the execution of troubleshooting tasks.
It runs commands trough ssh in multiples host.

### Taksks ###

* get-publisher-info
* get-publisher-logs
* snmpwalk
* snmptest
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How to install it? ###

Since fabric has a lot of dependencies, AuToPub is packaged in a docker container, so you can user the Dockerfile to build your container and run it. 

### How to run it ###

* List of available tasks

/home/autopub # fab --list
Available tasks:

  get-publisher-info   Show concurrent connections, time & date, etc.
  get-publisher-logs   Fetch logs for attaching to a support ticket
  snmptest             Do snmp walk for each OID documented
  snmpwalk             Fetch all availabe OIDs

* Execute arbitrary commands
fab --prompt-for-login-password -H centos@172.20.22.36 -- uname -a
fab --prompt-for-login-password -H centos@172.20.22.36 -- cat logs/agent.txt | grep Cleaning

* Run an specific task
fab --prompt-for-login-password -H centos@172.20.22.36 snmpwalk


### Uploading & Downloading files ###

Use python simple http server
