from fabric import task
from datetime import datetime

@task
def get_publisher_logs(c):
    '''Fetch logs for attaching to a support ticket '''
    now = datetime.now().isoformat(timespec='minutes')
    logfilename = "NSPublisher-" + c.host.replace(".","-") + "-" + now + ".tgz"
    
    c.run("sudo tar -czf /tmp/" + logfilename + " /home/centos/logs /home/centos/resources/.")
    c.get("/tmp/" + logfilename)
    c.run("sudo rm -f /tmp/" + logfilename)
    print("* Logfile " + logfilename + " seccesfully fetched.")

@task
def get_publisher_info(c):
    '''Show concurrent connections, time & date, etc. '''	
    print("\nINFO FOR PUBLISHER: ")
    c.run("sudo cat /home/centos/resources/publisherid")
    print( " @ " + c.host + "\n" + "-" *80 )
    print("\n>> DATE & TIME")
    c.run("timedatectl")
    print("\n>> CONNECTIONS")
    c.run("netstat -n | awk '/^tcp/ {++S[$NF]} END {for(a in S) print a, S[a]}'")

@task
def snmpwalk(c):
    '''Fetch all availabe OIDs'''
    ##-On will show numeric OIDs, while -Of will show full path of OIDs
    print("\nSNMPWALK TEST: ")
    c.run("sudo systemctl restart snmpd.service")
    c.run("sudo cat /home/centos/resources/publisherid")
    print( " @ " + c.host + "\n" + "-" *80 )
    snmpwalk = c.run("snmpwalk -On -v2c localhost -c netskope .1.3.6.1.4.1")
 

@task
def snmptest(c):
    '''Do snmp walk for each OID documented'''
    print("\nSNMPWALK TEST: ")
    c.run("sudo systemctl restart snmpd.service")
    c.run("sudo cat /home/centos/resources/publisherid")
    print( " @ " + c.host + "\n" + "-" *80 )
    print("\nDisk OIDs: ")
    print("\n>> Available space on the disk:")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.4.1.2021.9.1.7.1")
    print("\n>> Used space on the disk:")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.4.1.2021.9.1.8.1") 
    print("\n>> Percentage of space used on disk:")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.4.1.2021.9.1.9.1")
    print("\n>> Percentage of inodes used on disk:")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.4.1.2021.9.1.10.1")
    print("\n>> Path where the disk is mounted:")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.4.1.2021.9.1.2.1")
    print("\n>> Path of the device for the partition:")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.4.1.2021.9.1.3.1")
    print("\n>> Total size of the disk/partion (kBytes):")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.4.1.2021.9.1.6.1")

    print("\nCPU OIDs: ")
    print("\n>> Percentage of user CPU time:")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.4.1.2021.11.9.0")
    print("\n>> Raw user CPU time:")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.4.1.2021.11.50.0")
    print("\n>> Percentage of system CPU time:")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.4.1.2021.11.10.0")
    print("\n>> Raw system CPU time:")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.4.1.2021.11.52.0")
    print("\n>> Percentage of idle CPU time:")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.4.1.2021.11.11.0")
    print("\n>> Raw idle CPU time:")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.4.1.2021.11.53.0")

    print("\nRAM OIDs: ")
    print("\n>> Total RAM in machine:")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.4.1.2021.4.5.0")
    print("\n>> Total RAM used:")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.4.1.2021.4.6.0")
    print("\n>> Total RAM free:")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.4.1.2021.4.11.0")
    
    print("\nInterface OIDs:")
    print("\n>> Total bytes received on the interface:")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.2.1.2.2.1.10")
    print("\n>> Total bytes transmitted on the interface:")
    c.run("snmpwalk -v2c localhost -c netskope .1.3.6.1.2.1.2.2.1.16")
